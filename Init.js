import React, { Component } from 'react';
import { connect } from 'react-redux';
import SplashScreen from 'react-native-splash-screen';
import { StackActions, NavigationActions } from 'react-navigation';

class Init extends Component {

    componentDidMount() {

        SplashScreen.hide();
        this.init();
    }

    init = async () => {

        let route = 'Login';

        if (this.props.api_token == null) {

            route = 'Swipes';

        } else {

            if (this.props.api_token != 'not_set')
                route = 'Menu';
        }

        this.props.navigation.dispatch(StackActions.reset({ index: 0, key: null, actions: [NavigationActions.navigate({ routeName: route })] }));
    }

    render() { return null }
}


const mapStateToProps = state => ({ ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };
export default connect(mapStateToProps, mapDispatchToProps)(Init);
