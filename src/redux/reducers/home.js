import { HOME } from '../types';

const initialState = {

    account_number: '',
    company: '',
    savings_balance: '',
    loan_next_payment_date: '',
    loan_outstanding_balance: '',
    show_loans: '',
    transactions: '',
    active_loans_count: 0,
    loaded: false
}

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case HOME:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        default:
            return state;
    }
}