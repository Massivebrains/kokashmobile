import { PRODUCTS } from '../types';

const initialState = {

    products: [],
    cart: [],
    total: 0,
    loaded: false
}

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case PRODUCTS:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        default:
            return state;
    }
}