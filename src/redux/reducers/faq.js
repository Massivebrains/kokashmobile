import { FAQ } from '../types';

const initialState = {

    faqs: [],
    loaded: false
}

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case FAQ:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        default:
            return state;
    }
}