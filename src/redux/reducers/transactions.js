import { TRANSACTIONS } from '../types';

const initialState = {

    transactions: [],
    loaded: false
}

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case TRANSACTIONS:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        default:
            return state;
    }
}