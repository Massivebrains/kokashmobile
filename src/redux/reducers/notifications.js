import { NOTIFICATIONS } from '../types';

const initialState = {

    notifications: [],
    unread_count: 0,
    loaded: false
}

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case NOTIFICATIONS:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        default:
            return state;
    }
}