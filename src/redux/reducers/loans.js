import { LOANS } from '../types';

const initialState = {

    loans: [],
    loaded: false
}

export default function (state = initialState, action) {

    let { type, payload } = action;

    switch (type) {

        case LOANS:
            if (!payload)
                return { ...state, ...initialState };
            return { ...state, ...payload };

        default:
            return state;
    }
}