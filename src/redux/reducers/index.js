import { combineReducers } from 'redux';

import auth from './auth';
import faq from './faq';
import home from './home';
import loans from './loans';
import notifications from './notifications';
import products from './products';
import statements from './statements';
import transactions from './transactions';

export default combineReducers({

    auth,
    faq,
    home,
    loans,
    notifications,
    products,
    statements,
    transactions

})