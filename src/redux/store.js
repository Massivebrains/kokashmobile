import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

const persistConfig = {

    key: 'root',
    storage
}

const persistedReducer = persistReducer(persistConfig, rootReducer);
const middlewares = [thunk];
const compose = () => { };
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(persistedReducer, undefined, composeEnhancers(applyMiddleware(...middlewares)))
export const persistor = persistStore(store);