import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, TouchableOpacity, Alert, ScrollView } from 'react-native';
import { Text } from 'native-base';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import Head from '../components/Head';
import Spinner from '../components/Spinner';
import API from '../utils/API';
import Utils from '../utils/Utils';
import Colors from '../utils/Colors';

//user 
class LoanDetails extends Component {

    constructor(props) {

        super(props);

        this.state = {

            loaded: false,
            principal: '0',
            amount: '0',
            disbursed: '0',
            paid: '',
            outstanding: '',
            rate: '0',
            name: 'Loan Details',
            outstanding: '',
            transactions: []
        }
    }

    async componentDidMount() {

        this.load();
    }

    load = async () => {

        try {

            if (this.props.navigation.state.params.loan) {

                let loan = this.props.navigation.state.params.loan;
                this.setState({ loan: loan });

                if (loan.name) this.setState({ name: loan.name });
                if (loan.principal) this.setState({ principal: loan.principal });
                if (loan.disbursed) this.setState({ disbursed: loan.disbursed });
                if (loan.amount) this.setState({ amount: loan.amount });
                if (loan.paid) this.setState({ paid: loan.paid });
                if (loan.rate) this.setState({ rate: loan.rate + '%' });
                if (loan.outstanding) this.setState({ outstanding: loan.outstanding });

                let response = await API.get(`loans/transactions/${loan.id}`, this.props.api_token);

                if (response.status == false) {

                    Utils.toast(response.data);
                    this.setState({ loaded: true });

                } else {

                    this.setState({ transactions: response.data, loaded: true });
                }

            }


        } catch (ex) {

            this.setState({ loaded: true });
            Utils.toast();
        }

    }

    render() {

        if (this.state.loaded == false) return (<Spinner title='Loan Details' navigation={this.props.navigation} back='Pay' />);

        return (

            <ScrollView style={{ backgroundColor: Colors.white }}>

                <Head title={this.state.name} navigation={this.props.navigation} back='Loans' />

                <View style={styles.box}>
                    <View>
                        <Text style={styles.p}>Principal</Text>
                        <Text style={styles.h1}>{this.state.principal}</Text>
                    </View>

                    <View>
                        <Text style={styles.p}>Disbursed</Text>
                        <Text style={styles.h1}>{this.state.disbursed}</Text>
                    </View>
                </View>

                <View style={styles.box}>
                    <View>
                        <Text style={styles.p}>Amount Payable</Text>
                        <Text style={styles.h1}>{this.state.amount}</Text>
                    </View>

                    <View>
                        <Text style={styles.p}>Outstanding</Text>
                        <Text style={styles.h1}>{this.state.outstanding}</Text>
                    </View>
                </View>

                {this.state.transactions.map(row => {

                    let color = '#E8AF01';
                    if (row.status == 'PENDING') color = Colors.red;
                    if (row.status == 'COMPLETED') color = Colors.green;

                    let icon = row.status == 'PENDING' ? 'history' : 'done-all';

                    return (

                        <TouchableOpacity key={row.id} style={[styles.payment, { backgroundColor: color }]} onPress={() => Alert.alert('', `${row.description}`)}>
                            <Text style={{ fontSize: 18, color: Colors.white }}><Icon name={`${icon}`} style={{ fontSize: 16 }} />&nbsp;{row.month}</Text>
                            <Text style={styles.paymentDate}>{row.status} {row.status == 'Completed' ? ` | Paid on ${row.payment_date}` : ` | Due on ${row.due_date}`}</Text>
                        </TouchableOpacity>
                    )
                })}

                <Text note style={{ textAlign: 'center', paddingLeft: 40, paddingRight: 40, marginTop: 20, marginBottom: 20, color: Colors.black }}>
                    Please note that Charges from Payment Providers are not included
                </Text>

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flexDirection: 'row'
    },
    h1: {
        color: Colors.black,
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    p: {
        textAlign: 'center',
        color: Colors.darkBlue
    },
    box: {
        flexDirection: 'row',
        padding: 10,
        marginTop: 20, justifyContent: 'space-around',
        borderBottomWidth: 1,
        borderBottomColor: Colors.grey
    },
    payment: {
        flexDirection: 'row',
        padding: 10,
        margin: 3,
        borderRadius: 2,
        justifyContent: 'space-between'
    },
    paymentDate: {
        justifyContent: 'flex-end',
        color: Colors.white,
        fontStyle: 'italic'
    },
    small: {
        textAlign: 'center',
        color: Colors.darkGrey,
        fontSize: 15
    },
    big: {
        textAlign: 'center',
        fontSize: 18
    },
    listItem: {
        flex: 1
    },
    th: {
        color: Colors.black,
        fontWeight: 'normal',
        fontSize: 13,
        padding: 0,
        margin: 0,
        textAlign: 'center'
    },
    td: {
        color: Colors.black,
        fontSize: 11,
        padding: 0,
        margin: 0,
        textAlign: 'center'
    }
});


const mapStateToProps = state => ({ ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(LoanDetails);