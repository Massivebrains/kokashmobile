import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, FlatList, Image, RefreshControl, ScrollView } from 'react-native';
import { Text } from 'native-base';
import Head from '../components/Head';
import Spinner from '../components/Spinner';
import Transaction from '../components/Transaction';
import API from '../utils/API';
import Utils from '../utils/Utils';
import Colors from '../utils/Colors';
import { TRANSACTIONS } from '../redux/types';

class Transactions extends Component {

    constructor(props) {

        super(props);

        this.state = { refreshing: false };
    }

    setProps = (props = {}) => {

        this.props.dispatch({ type: TRANSACTIONS, payload: props });
    }

    async componentDidMount(refresh = false) {

        try {

            if (refresh) this.setState({ refreshing: true });

            let response = await API.get('transactions', this.props.api_token);

            if (response.status == false) {

                Utils.toast(response.data);

            } else {

                this.setProps({ transactions: response.data, loaded: true });
            }

            if (refresh) setTimeout(() => this.setState({ refreshing: false }), 1000);

        } catch (ex) {

            Utils.toast(ex.message);
        }
    }

    render() {

        if (this.props.transactions.loaded == false) return (<Spinner title='Transactions' navigation={this.props.navigation} back='Home' />);

        return (

            <ScrollView refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this.componentDidMount(true)} />} style={{ backgroundColor: Colors.white }}>

                <Head title='Transactions' navigation={this.props.navigation} back='Home' />

                {this.props.transactions.length
                    ? null
                    : <Image source={require('../images/no-data.png')} style={{ width: 400, height: 400, alignSelf: 'center', }} />
                }

                {this.props.transactions.length
                    ? <FlatList noPadding data={this.props.transactions} renderItem={({ item }) => <Transaction transaction={item} navigation={this.props.navigation} />} />
                    : <Text style={{ marginTop: 40, textAlign: 'center', padding: 30, fontSize: 18, color: Colors.light, fontStyle: 'italic' }}>When you carry out any transaction. They would show up here.</Text>
                }

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end'
    }
});


const mapStateToProps = state => ({ ...state.auth, ...state.transactions });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(Transactions);