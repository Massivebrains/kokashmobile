import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, TouchableOpacity } from 'react-native';
import { Body, H1, Icon, Text, ListItem, Left, Right, Separator, Thumbnail, Item, Label, Input, Picker } from 'native-base';
import Head from '../components/Head';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import * as Animatable from 'react-native-animatable';
import PhotoUpload from 'react-native-photo-upload';
import { SkypeIndicator } from 'react-native-indicators';
import API from '../utils/API';
import KButton from '../components/KButton';
import Utils from '../utils/Utils';
import Colors from '../utils/Colors';
import { AUTH } from '../redux/types';

class Profile extends Component {

    constructor(props) {

        super(props);

        this.state = {

            user: {},
            cards: [],
            partners: [],
            uploading: false,
            photo: null,
            loading: false,

            first_name: '',
            last_name: '',
            phone: '',
            email: '',
            occupation: '',
            address: '',
            employer: '',
            employer_address: '',
            next_of_kin: '',
            bank_id: 0,
            bank_account_name: '',
            bank_account_number: '',
            banks: [],
            edit: false
        }
    }

    setProps = (props = {}) => {

        this.props.dispatch({ type: AUTH, payload: props });
    }

    async componentDidMount() {

        this.load();
        this.props.navigation.addListener('willFocus', this.load);
    }

    load = async () => {

        try {

            let user = this.props.user;

            this.setState({
                user: user,
                photo: user.photo,
                first_name: user.first_name,
                last_name: user.last_name,
                phone: user.phone,
                email: user.email,
                occupation: user.occupation,
                address: user.address,
                employer: user.employer,
                employer_address: user.employer_address,
                next_of_kin: user.next_of_kin
            });

            let response = await API.get('profile', this.props.api_token);

            if (response.status == true) {

                let data = response.data;

                this.setState({
                    user: data,
                    cards: data.debitcards ? data.debitcards : [],
                    partners: data.partners ? data.partners : []
                });

                this.setProps({ user: data });

            }

            let banks_response = await API.get('banks', this.props.api_token);

            if (banks_response.status == true)
                this.setProps({ banks: banks_response.data });

        } catch (ex) {

            Utils.toast(ex.message);
        }

    }

    async editProfile() {

        try {

            this.setState({ loading: true });
            let response = await API.post('edit-profile', this.state);

            if (response.status == true) {

                let { data } = response;

                this.setProps({ user: data });

                this.setState({

                    user: data,
                    first_name: data.first_name,
                    last_name: data.last_name,
                    phone: data.phone,
                    email: data.email,
                    occupation: data.occupation,
                    address: data.address,
                    employer: data.employer,
                    employer_address: data.employer_address,
                    next_of_kin: data.next_of_kin
                });

                this.setState({ edit: false, loading: false });

            } else {

                this.setState({ loading: false, edit: false });

            }

        } catch (ex) {

            this.setState({ loading: false, edit: false });
        }
    }

    async upload(image) {

        try {

            let response = await API.post('upload-photo', { image: image }, this.props.api_token);

            if (response) {

                if (response.status == true) {

                    this.setProps({ user: response.data });

                } else {

                    Utils.toast(response.data.toString());

                }

                setTimeout(() => this.setState({ uploading: false }), 1000);

            }

        } catch (ex) {

            this.setState({ uploading: false });
            Utils.toast(ex.message);
        }

    }

    render() {

        return (

            <KeyboardAwareScrollView contentContainerStyle={{ backgroundColor: Colors.white }} resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={true}>

                <Head title='Profile' navigation={this.props.navigation} back='Home' />

                {this.state.edit
                    ? null
                    : <View style={{ height: 180, backgroundColor: Colors.darkBlue }}>
                        <PhotoUpload onPhotoSelect={image => this.upload(image)} onResponse={() => this.setState({ uploading: true })}>
                            {this.state.uploading
                                ? <SkypeIndicator color={Colors.white} />
                                : this.state.photo
                                    ? <Thumbnail large source={{ uri: this.props.user.photo }} style={{ alignSelf: 'center' }} />
                                    : <Thumbnail large source={require('../images/avatar.png')} style={{ alignSelf: 'center' }} />}

                        </PhotoUpload>

                        <H1 style={{ color: Colors.white, marginTop: 10, fontSize: 32, textAlign: 'center' }}>{this.props.user.name}</H1>
                        <Text style={{ color: Colors.white, marginTop: 2, marginBottom: 5, fontSize: 12, textAlign: 'center' }}>{this.props.user.company_name}</Text>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', paddingTop: 5 }}>
                            <TouchableOpacity style={{ marginTop: 2, marginBottom: 10 }} onPress={() => this.setState({ edit: !this.state.edit })}>
                                <Text style={{ color: Colors.white, fontSize: 15, textAlign: 'center' }}>Edit Profile</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ marginTop: 2, marginBottom: 10 }} onPress={() => this.props.navigation.navigate('Password')}>
                                <Text style={{ color: Colors.white, fontSize: 15, textAlign: 'center' }}>Change Password</Text>
                            </TouchableOpacity>
                        </View>

                    </View>}

                {this.state.edit
                    ? <Animatable.View animation='fadeIn' easing='ease-out' iterationCount={1} style={{ margin: 0, padding: 10 }}>

                        <Item floatingLabel style={{ marginBottom: 5, marginTop: 15 }}>
                            <Label style={{ marginTop: 5 }}>First Name</Label>
                            <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.setState({ first_name: text })} value={this.state.first_name} />
                        </Item>

                        <Item floatingLabel style={{ marginBottom: 5, marginTop: 5 }}>
                            <Label style={{ marginTop: 5 }}>Last Name</Label>
                            <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.setState({ last_name: text })} value={this.state.last_name} />
                        </Item>

                        <Item floatingLabel style={{ marginBottom: 5, marginTop: 5 }}>
                            <Label style={{ marginTop: 5 }}>Email Address</Label>
                            <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.setState({ email: text })} value={this.state.email} />
                        </Item>

                        <Item floatingLabel style={{ marginBottom: 5, marginTop: 5 }}>
                            <Label style={{ marginTop: 5 }}>Phone Number</Label>
                            <Input style={{ marginTop: 5 }} keyboardType='numeric' selectionColor={Colors.primary} onChangeText={text => this.setState({ phone: text })} value={this.state.phone} />
                        </Item>

                        <Item floatingLabel style={{ marginBottom: 5, marginTop: 5 }}>
                            <Label style={{ marginTop: 5 }}>Address</Label>
                            <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.setState({ address: text })} value={this.state.address} />
                        </Item>

                        <Item floatingLabel style={{ marginBottom: 5, marginTop: 5 }}>
                            <Label style={{ marginTop: 5 }}>Profession</Label>
                            <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.setState({ occupation: text })} value={this.state.occupation} />
                        </Item>

                        <Item floatingLabel style={{ marginBottom: 5, marginTop: 5 }}>
                            <Label style={{ marginTop: 5 }}>Employer</Label>
                            <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.setState({ employer: text })} value={this.state.employer} />
                        </Item>

                        <Item floatingLabel style={{ marginBottom: 5, marginTop: 5 }}>
                            <Label style={{ marginTop: 5 }}>Employer Address</Label>
                            <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.setState({ employer_address: text })} value={this.state.employer_address} />
                        </Item>

                        <Item floatingLabel style={{ marginBottom: 5, marginTop: 5 }}>
                            <Label style={{ marginTop: 5 }}>Next Of Kin</Label>
                            <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.setState({ next_of_kin: text })} value={this.state.next_of_kin} />
                        </Item>

                        {this.props.banks
                            ? <Picker mode='dropdown' placeholder='Bank Name' placeholderStyle={{ color: Colors.black }} placeholderIconColor={Colors.black} style={{ width: undefined }} selectedValue={this.state.bank_id} onValueChange={value => { this.setState({ bank_id: value }) }}>

                                <Picker.Item label='Select Bank' value='0' />

                                {this.props.banks.map(row => (

                                    <Picker.Item key={row.id} label={row.name} value={row.id} />
                                ))}
                            </Picker>
                            : null}

                        <Item floatingLabel style={{ marginBottom: 5, marginTop: 5 }}>
                            <Label style={{ marginTop: 5 }}>Bank Account Name</Label>
                            <Input style={{ marginTop: 5 }} selectionColor={Colors.primary} onChangeText={text => this.setState({ bank_account_name: text })} value={this.state.bank_account_name} />
                        </Item>

                        <Item floatingLabel style={{ marginBottom: 5, marginTop: 5 }}>
                            <Label style={{ marginTop: 5 }}>Bank Account Number</Label>
                            <Input style={{ marginTop: 5 }} keyboardType='numeric' selectionColor={Colors.primary} onChangeText={text => this.setState({ bank_account_number: text })} value={this.state.bank_account_number} />
                        </Item>

                        <KButton onPress={() => this.editProfile()} text='SUBMIT' loading={this.state.loading} background={Colors.darkBlue} color={Colors.white}></KButton>

                    </Animatable.View>


                    : <Animatable.View animation='fadeIn' easing='ease-out' iterationCount={1} style={{ margin: 0, backgroundColor: Colors.white }}>

                        <Separator style={{ marginBottom: 10 }}>
                            <Text style={{ fontSize: 15 }}>Personal Information</Text>
                        </Separator>

                        <ListItem icon noBorder>
                            <Left>
                                <Icon active name='contact' style={{ color: Colors.darkBlue }} />
                            </Left>
                            <Body>
                                <Text>Name</Text>
                            </Body>
                            <Right>
                                <Text>{this.props.user.name}</Text>
                            </Right>
                        </ListItem>

                        <ListItem icon noBorder >
                            <Left>
                                <Icon active name='mail' style={{ color: Colors.darkBlue }} />
                            </Left>
                            <Body>
                                <Text>Email</Text>
                            </Body>
                            <Right>
                                <Text >{this.props.user.email}</Text>
                            </Right>
                        </ListItem>

                        <ListItem icon noBorder >
                            <Left>
                                <Icon active name='call' style={{ color: Colors.darkBlue }} />
                            </Left>
                            <Body>
                                <Text>Phone</Text>
                            </Body>
                            <Right>
                                <Text>{this.props.user.phone}</Text>
                            </Right>
                        </ListItem>

                        <Separator style={{ marginTop: 20, marginBottom: 10 }}>
                            <Text style={{ fontSize: 15 }}>Bank Account Information</Text>
                        </Separator>

                        <ListItem icon noBorder >
                            <Left>
                                <Icon active name='home' style={{ color: Colors.darkBlue }} />
                            </Left>
                            <Body>
                                <Text>Bank</Text>
                            </Body>
                            <Right>
                                <Text >{this.props.user.bank_name}</Text>
                            </Right>
                        </ListItem>

                        <ListItem icon noBorder >
                            <Left>
                                <Icon active name='wifi' style={{ color: Colors.darkBlue }} />
                            </Left>
                            <Body>
                                <Text>Account Name</Text>
                            </Body>
                            <Right>
                                <Text >{this.props.user.bank_account_name}</Text>
                            </Right>
                        </ListItem>

                        <ListItem icon noBorder >
                            <Left>
                                <Icon active name='keypad' style={{ color: Colors.darkBlue }} />
                            </Left>
                            <Body>
                                <Text>Account Number</Text>
                            </Body>
                            <Right>
                                <Text >{this.props.user.bank_account_number}</Text>
                            </Right>
                        </ListItem>

                        {this.state.cards.length
                            ? <Separator style={{ marginTop: 20, marginBottom: 10 }}>
                                <Text style={{ fontSize: 15 }}>Linked Debit Cards</Text>
                            </Separator>
                            : <View style={{ backgrounColor: Colors.white, height: 100 }}></View>
                        }


                        {this.state.cards.map(row => {

                            return (

                                <ListItem icon noBorder key={row.id}>

                                    <Left>
                                        <Icon active name='card' style={{ color: Colors.darkBlue }} />
                                    </Left>
                                    <Body>
                                        <Text>{row.card_type}</Text>
                                        <Text note>{row.bank} | {row.exp_month}/{row.exp_year}</Text>
                                    </Body>
                                    <Right>
                                        <Text>...{row.last4}</Text>
                                    </Right>
                                </ListItem>
                            )

                        })}

                        {this.state.partners.length
                            ? <Separator style={{ marginTop: 20 }}>
                                <Text style={{ fontSize: 15 }}>Beneficiaries</Text>
                            </Separator>
                            : null
                        }

                        {this.state.partners.map(row => {

                            return (

                                <ListItem icon noBorder key={row.id} >
                                    <Left>
                                        <Icon active name='person' style={{ color: Colors.darkBlue }} />
                                    </Left>
                                    <Body>
                                        <Text>{row.name}</Text>
                                    </Body>
                                    <Right>
                                        <Text>{row.phone}</Text>
                                    </Right>
                                </ListItem>
                            )
                        })}

                    </Animatable.View>

                }

            </KeyboardAwareScrollView>
        );
    }
}


const mapStateToProps = state => ({ ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(Profile);