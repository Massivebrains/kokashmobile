import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Alert } from 'react-native';
import { Item, Label, Input } from 'native-base';
import Head from '../../components/Head';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { StackActions, NavigationActions } from 'react-navigation';
import API from '../../utils/API';
import KButton from '../../components/KButton';
import Colors from '../../utils/Colors';

//user here
class Password extends Component {

    constructor(props) {

        super(props);

        this.state = {

            loading: false,
            password: '',
            new_password: '',
            password_confirmation: ''
        }
    }

    async changePassword() {

        try {

            this.setState({ loading: true });
            let response = await API.post('change-password', this.state, this.props.api_token);

            Alert.alert('', response.data);
            this.setState({ loading: false, password: '', new_password: '', password_confirmation: '' });

            if (response.status == true)
                this.props.navigation.dispatch(StackActions.reset({ index: 0, key: null, actions: [NavigationActions.navigate({ routeName: 'Menu' })] }));

        } catch (ex) {

            this.setState({ loading: false });
        }
    }

    render() {

        return (

            <KeyboardAwareScrollView contentContainerStyle={{ backgroundColor: Colors.white, height: '100%' }} resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={true}>

                <Head title='Change Password' navigation={this.props.navigation} back='Home' />

                <View style={{ margin: 0, padding: 10 }}>

                    <Item floatingLabel style={{ marginBottom: 5, marginTop: 15 }}>
                        <Label style={{ marginTop: 5 }}>Current Password</Label>
                        <Input style={{ marginTop: 5 }} secureTextEntry={true} selectionColor={Colors.primary} onChangeText={text => this.setState({ password: text })} value={this.state.password} />
                    </Item>

                    <Item floatingLabel style={{ marginBottom: 5, marginTop: 15 }}>
                        <Label style={{ marginTop: 5 }}>New Password</Label>
                        <Input style={{ marginTop: 5 }} secureTextEntry={true} selectionColor={Colors.primary} onChangeText={text => this.setState({ new_password: text })} value={this.state.new_password} />
                    </Item>

                    <Item floatingLabel style={{ marginBottom: 5, marginTop: 5 }}>
                        <Label style={{ marginTop: 5 }}>Confirm New Password</Label>
                        <Input style={{ marginTop: 5 }} secureTextEntry={true} selectionColor={Colors.primary} onChangeText={text => this.setState({ password_confirmation: text })} value={this.state.password_confirmation} />
                    </Item>

                    <KButton onPress={() => this.changePassword()} text='SUBMIT' loading={this.state.loading} background={Colors.darkBlue} color={Colors.white}></KButton>

                </View>

            </KeyboardAwareScrollView>
        );
    }
}

const mapStateToProps = state => ({ ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(Password);