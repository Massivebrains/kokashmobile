import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export default class Button extends Component {

    render({ onPress } = this.props) {

        return (

            <TouchableOpacity onPress={onPress}>
                <View>
                    <Text style={styles.text}>{this.props.text.toUpperCase()}</Text>
                </View>
            </TouchableOpacity>

        );
    }
}

const styles = StyleSheet.create({

    text: {
        color: '#1c3d5a',
        fontWeight: 'bold',
        fontSize: 16
    },

});