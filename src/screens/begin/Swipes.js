import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text, View, AsyncStorage, Image } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import Swiper from './Swiper';
import Colors from '../../utils/Colors';
import { AUTH } from '../../redux/types';

class Swipes extends Component {

  componentDidMount() {

    this.props.dispatch({ type: AUTH, payload: { api_token: 'not_set' } });
    SplashScreen.hide();

  }

  render() {

    return (

      <Swiper navigation={this.props.navigation}>

        <View style={[styles.slide, { backgroundColor: Colors.white }]}>

          <Image resizeMode='contain' style={{ width: 300, height: 300 }} source={require('../../images/manage-money.png')} />
          <Text style={styles.header}>WELCOME TO KOKASH!</Text>
          <Text style={styles.text}>KoKash is here to help! Manage your cooperative account with our seemless system and dedicated administrative solution.</Text>

        </View>

        <View style={[styles.slide, { backgroundColor: Colors.white }]}>

          <Image resizeMode="contain" style={{ width: 300, height: 300 }} source={require('../../images/what-can-i-do.png')} />
          <Text style={styles.header}>WHAT CAN I DO?</Text>
          <Text style={styles.text}>View your Transactions, Manage and Apply for Loans, Get Notified on activites concerning your account and lots more!</Text>

        </View>

        <View style={[styles.slide, { backgroundColor: Colors.white }]}>

          <Image resizeMode="contain" style={{ width: 300, height: 300 }} source={require('../../images/every-time.png')} />
          <Text style={styles.header}>AVALIABILITY</Text>
          <Text style={styles.text}>24Hours Service Everywhere on all Devices.</Text>

        </View>

      </Swiper>

    );
  }
}

const styles = StyleSheet.create({

  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    color: Colors.darkBlue,
    fontFamily: 'Avenir',
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 15,
    marginTop: 50,
  },
  text: {
    color: Colors.darkBlue,
    fontFamily: 'Avenir',
    fontSize: 14,
    marginHorizontal: 40,
    textAlign: 'center',
  },

});

const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(null, mapDispatchToProps)(Swipes);


