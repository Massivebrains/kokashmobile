import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, StyleSheet, StatusBar, Alert, View, TouchableOpacity, Linking } from 'react-native';
import { Item, Input } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import KButton from '../../components/KButton';
import API from '../../utils/API';
import Colors from '../../utils/Colors';
import { StackActions, NavigationActions } from 'react-navigation';
import { AUTH } from '../../redux/types';

class Login extends Component {

    constructor(props) {

        super(props);

        this.state = {

            phone: '',
            password: '',
            company: '',
            loading: false,
            editable: true
        };
    }

    submit = async () => {

        try {

            this.setState({ button: 'Requesting...', editable: false, loading: true });

            let response = await API.post('login', this.state);

            let { status, data } = response;

            if (status == true) {

                this.props.dispatch({

                    type: AUTH,
                    payload: {
                        api_token: data.api_token,
                        user: data,
                        name: data.name,
                        company: data.company.name
                    }
                });

                this.setState({ loading: false });
                let route = data.last_login_at ? 'Menu' : 'Password';

                this.props.navigation.dispatch(StackActions.reset({ index: 0, key: null, actions: [NavigationActions.navigate({ routeName: route })] }));

            } else {

                this.setState({ button: 'Continue', editable: true, password: '', loading: false });
                Alert.alert('Login', response.data);

            }

        } catch (ex) {

            this.setState({ button: 'Continue', editable: true, loading: false, password: '' });
            Alert.alert('Login', ex.toString());
        }

    }

    render() {

        return (

            <KeyboardAwareScrollView contentContainerStyle={styles.container} resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={true}>

                <StatusBar backgroundColor={Colors.white} barStyle='dark-content' />

                <View style={{ alignItems: 'flex-start', margin: 20 }}>
                    <Text style={{ fontSize: 60, fontWeight: 'bold', paddingTop: 30, color: Colors.darkBlue }}>KoKash</Text>
                    <Text style={{ color: Colors.darkBlue }}>Lets Get Started...</Text>
                </View>

                <View style={{ marginTop: 50, padding: 10 }}>

                    <Item style={styles.item}>
                        <Icon active name='phone' style={{ color: Colors.darkGrey, fontSize: 18 }} />
                        <Input onChangeText={phone => this.setState({ phone: phone })} editable={this.state.editable} value={this.state.phone} placeholder='Phone Number' style={styles.input} placeholderTextColor={Colors.greyDark1} selectionColor={Colors.blue} keyboardType='numeric' />
                    </Item>

                    <Item style={styles.item}>
                        <Icon active name='lock' style={{ color: Colors.darkGrey, fontSize: 18 }} />
                        <Input onChangeText={password => this.setState({ password: password })} editable={this.state.editable} value={this.state.password} placeholder='Password' style={styles.input} secureTextEntry={true} placeholderTextColor={Colors.greyDark1} selectionColor={Colors.blue} />
                    </Item>

                    <Item style={styles.item}>
                        <Icon active name='home' style={{ color: Colors.darkGrey, fontSize: 18 }} />
                        <Input onChangeText={company => this.setState({ company: company })} editable={this.state.editable} value={this.state.company} placeholder='Cooperative Name' style={styles.input} placeholderTextColor={Colors.greyDark1} selectionColor={Colors.blue} />
                    </Item>

                    <KButton onPress={() => this.submit()} text='CONTINUE' loading={this.state.loading} background={Colors.darkBlue} color={Colors.white} />
                    <TouchableOpacity onPress={() => Linking.openURL('https://kokash.ng/webview/password-reset')}>
                        <Text note style={{ textAlign: 'center', color: Colors.black, elevation: 1, marginTop: 10, fontSize: 14 }}>
                            Forgot Password?
                        </Text>
                    </TouchableOpacity>
                </View>

                <View style={{ position: 'absolute', top: '90%' }}>
                    <TouchableOpacity onPress={() => Linking.openURL('https://kokash.ng/terms')}>
                        <Text note style={{ textAlign: 'center', color: Colors.black, elevation: 1, marginTop: 10, fontSize: 12 }}>
                            By continuing, I confirm that i have read and agreed to the <Text style={{ color: Colors.red }}>Terms &amp; Conditions</Text> and <Text style={{ color: Colors.red }}>Privacy Policy</Text>
                        </Text>
                    </TouchableOpacity>
                </View>

            </KeyboardAwareScrollView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: Colors.white,
        height: '100%'
    },
    item: {
        backgroundColor: Colors.grey,
        paddingLeft: 10,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 15,
        borderWidth: 0.5,
        borderColor: Colors.grey
    },
    input: {
        color: Colors.black
    },
    login: {
        marginLeft: 10,
        marginRight: 10,
        height: 55,
        backgroundColor: Colors.darkBlue
    },
    footer: {

    }
});

const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(null, mapDispatchToProps)(Login);