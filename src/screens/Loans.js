import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, FlatList, Image, Text, View } from 'react-native';
import Head from '../components/Head';
import Loan from '../components/Loan';
import Spinner from '../components/Spinner';
import API from '../utils/API';
import Utils from '../utils/Utils';
import Colors from '../utils/Colors';
import { LOANS } from '../redux/types';

class Loans extends Component {

    constructor(props) {

        super(props);
    }

    setProps = (props = {}) => {

        this.props.dispatch({ type: LOANS, payload: props });
    }

    async componentDidMount() {

        try {

            let response = await API.get('loans', this.props.api_token);

            if (response.status == false) {

                Utils.toast();

            } else {

                this.setProps({ loans: response.data, loaded: true });
            }

        } catch (ex) {

            Utils.toast(ex.message);
        }
    }

    render() {

        if (this.props.loaded == false) return (<Spinner title='Loans' navigation={this.props.navigation} back='Home' />);

        return (

            <View style={styles.container}>

                <Head title='Loans' navigation={this.props.navigation} back='Home' />

                <FlatList style={{ paddingLeft: 3, paddingRight: 3 }} data={this.props.loans} renderItem={({ item }) => <Loan loan={item} navigation={this.props.navigation} />} />

                {this.props.loans.length
                    ? null
                    : <Image source={require('../images/no-data.png')} style={{ width: 400, height: 400, alignSelf: 'center', }} />
                }

                {this.props.loans.length
                    ? null
                    : <Text style={{ color: Colors.light, fontSize: 15, fontStyle: 'italic', textAlign: 'center' }}> When you have a running loan, you can monitor your repayments here.</Text>
                }

            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1
    }
});

const mapStateToProps = state => ({ ...state.auth, ...state.loans });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(Loans);