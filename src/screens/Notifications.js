import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Text, Body, ListItem, Left, Right, Icon } from 'native-base';
import Head from '../components/Head';
import Colors from '../utils/Colors';
import Spinner from '../components/Spinner';
import API from '../utils/API';
import Utils from '../utils/Utils';
import { NOTIFICATIONS } from '../redux/types';

class Notifications extends Component {

    constructor(props) {

        super(props);
        this.state = { notifications: [] }

    }

    setProps = (props = {}) => {

        this.props.dispatch({ type: NOTIFICATIONS, payload: props });
    }

    async componentDidMount() {

        try {

            let response = await API.get('notifications', this.props.api_token);

            if (response.status == false) {

                Utils.toast();

            } else {

                this.setProps({ notifications: response.data, loaded: true });
            }

        } catch (ex) {

            Utils.toast(ex.message);
        }
    }

    render() {

        if (this.props.loaded == false) return (<Spinner title='Notifications' navigation={this.props.navigation} back='Home' />);

        return (

            <ScrollView style={styles.container}>

                <Head title='Notifications' navigation={this.props.navigation} back='Home' />

                {this.props.notifications.map(row => {

                    return (

                        <TouchableOpacity key={row.id}>
                            <ListItem avatar>
                                <Left>
                                    <Icon name='notifications' style={{ color: Colors.black }} />
                                </Left>
                                <Body>
                                    <Text style={{ color: Colors.black }}>{row.title}</Text>
                                    <Text note>{row.notification}</Text>
                                </Body>
                                <Right>
                                    <Text note>{row.scheduled_at}</Text>
                                </Right>
                            </ListItem>
                        </TouchableOpacity>
                    );
                })}

                {this.props.notifications.length
                    ? null
                    : <Image source={require('../images/no-data.png')} style={{ width: 400, height: 400, alignSelf: 'center', }} />
                }

                {this.props.notifications.length
                    ? null
                    : <Text style={{ marginTop: 30, textAlign: 'center', padding: 30, fontSize: 18, color: Colors.light, fontStyle: 'italic' }}>Notifications concerning your account would be avaliable here.</Text>
                }

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1
    },
    text: {
        flexWrap: 'wrap',
        color: Colors.black
    },
});


const mapStateToProps = state => ({ ...state.auth, ...state.notifications });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);