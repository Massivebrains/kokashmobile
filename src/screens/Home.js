import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, FlatList, Image, RefreshControl, ScrollView, View, TouchableOpacity, Alert } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Text, H3, H1, Separator } from 'native-base';
import { StackActions, NavigationActions } from 'react-navigation';
import Head from '../components/Head';
import Spinner from '../components/Spinner';
import Transaction from '../components/Transaction';
import Colors from '../utils/Colors';
import API from '../utils/API';
import Utils from '../utils/Utils';
import { HOME } from '../redux/types';

class Home extends Component {

    constructor(props) {

        super(props);

        this.state = { refreshing: false }
    }

    async componentDidMount() {

        this.load();
    }

    setProps = (props = {}) => {

        this.props.dispatch({ type: HOME, payload: props });
    }

    load = async () => {

        try {

            let network = await NetInfo.getConnectionInfo();

            if (network.type == 'none') {

                this.props.navigation.dispatch(StackActions.reset({ index: 0, key: null, actions: [NavigationActions.navigate({ routeName: 'Offline' })] }));
                return;
            }

            this.setState({ refreshing: true });

            let response = await API.get('dashboard', this.props.api_token);

            if (response.status == false) {

                Utils.toast();

            } else {

                let data = response.data;

                this.setProps({

                    company: data.company,
                    savings_account_number: data.savings_account_number,
                    savings_balance: data.savings_balance,
                    active_loans_count: data.active_lans_count,
                    active_loans_count: data.active_loans_count,
                    loan_outstanding_balance: data.loan_outstanding_balance,
                    loan_next_payment_date: data.loan_next_payment_date,
                    transactions: data.recent_transactions,
                    loaded: true
                });

                this.setState({ refreshing: false });
            }

        } catch (ex) {

            Alert.alert(ex.message);
            //Utils.toast();
        }

    }

    render() {

        if (this.props.loaded == false)
            return (<Spinner title='Home' navigation={this.props.navigation} />);

        return (

            <ScrollView refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this.load()} />} style={{ backgroundColor: Colors.white, flex: 1 }}>

                <Head title='Home' navigation={this.props.navigation} />

                <View>

                    <TouchableOpacity style={{ ...styles.card, backgroundColor: Colors.darkBlue }} onPress={() => this.props.navigation.navigate('Transactions')}>
                        <H3 style={styles.text}>SAVINGS ACCOUNT</H3>
                        <Text style={styles.text}>{this.props.company}</Text>
                        <H1 style={styles.h1}>{this.props.savings_balance}</H1>
                        <Text style={styles.text}>Savings Balance</Text>
                    </TouchableOpacity>

                    {this.props.active_loans_count > 0
                        ? <TouchableOpacity style={{ ...styles.card, backgroundColor: Colors.white }} onPress={() => this.props.navigation.navigate('Loans')}>
                            <H3 style={{ ...styles.text, color: Colors.darkBlue }}>LOANS</H3>
                            <Text style={{ ...styles.text, color: Colors.darkBlue }}>Next Due Date: {this.props.loan_next_payment_date}</Text>
                            <H1 style={{ ...styles.h1, color: Colors.darkBlue }}>{this.props.loan_outstanding_balance}</H1>
                            <Text style={{ ...styles.text, color: Colors.darkBlue }}>Outstanding Balance</Text>
                        </TouchableOpacity>
                        : null
                    }


                    {this.props.transactions.length
                        ? <Separator style={{ backgroundColor: Colors.grey }}>
                            <Text style={{ fontSize: 15, color: Colors.darkBlue }}>Recent Transactions</Text>
                        </Separator>
                        : null
                    }

                    <FlatList noPadding data={this.props.transactions} renderItem={({ item }) => <Transaction transaction={item} navigation={this.props.navigation} />} />

                    {this.props.transactions.length
                        ? null
                        : <Image source={require('../images/no-data.png')} style={{ width: 300, height: 300, alignSelf: 'center', }} />
                    }

                    {this.props.transactions.length
                        ? null
                        : <Text style={{ marginTop: 20, textAlign: 'center', padding: 30, fontSize: 18, color: Colors.light, fontStyle: 'italic' }}>Your Recent Transactions will show up here. For now, you have no recent transactions.</Text>
                    }
                </View>

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        // flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end'
    },
    indicator: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        height: '100%'
    },
    text: {
        color: Colors.darkBlue,
        alignSelf: 'center',
        color: Colors.white
    },
    h1: {
        marginTop: 10,
        color: Colors.white,
        fontWeight: 'bold',
        alignSelf: 'center'
    },
    card: {
        backgroundColor: Colors.white,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
        padding: 20,
        shadowRadius: 2,
        shadowOffset: {
            width: 0,
            height: -3
        },
        shadowColor: Colors.darkBlue,
        elevation: 4
    }
});

const mapStateToProps = state => ({ ...state.auth, ...state.home });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(Home);