import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, Alert } from 'react-native';
import Head from '../components/Head';
import { Text, Left, Right, Icon, Body, H3, DatePicker, Button, Card, CardItem, Separator, List, ListItem } from 'native-base';
import Colors from '../utils/Colors';
import API from '../utils/API';
import Utils from '../utils/Utils';
import { STATEMENTS } from '../redux/types';

class Statement extends Component {

    constructor(props) {

        super(props);
        this.state = { start_date: '', end_date: '', button: 'CONTINUE' }
    }

    setProps = (props = {}) => {

        this.props.dispatch({ type: STATEMENTS, payload: props });
    }

    async componentDidMount() {

        try {

            let response = await API.get('statements', this.props.api_token);

            if (response.status == false) {

                Utils.toast();

            } else {

                this.setProps({ statements: response.data, loaded: true });
            }

        } catch (ex) {

            Utils.toast();
        }
    }



    submit = async () => {

        try {

            if (this.state.start_date == '' || this.state.end_date == '') {

                Utils.toast('Please select a start and end date.');
                return;
            }

            this.setState({ button: 'Requesting...' });
            let start = new Date(this.state.start_date);
            let start_date = `${start.getFullYear()}-${start.getMonth() + 1}-${start.getDate()}`;
            let end = new Date(this.state.end_date);
            let end_date = `${end.getFullYear()}-${end.getMonth() + 1}-${end.getDate()}`;

            let response = await API.post('statement', { start_date: start_date, end_date: end_date }, this.props.user.api_token);

            if (response.status == false) {

                Alert.alert('Statement', response.data);
                this.setState({ button: 'CONTINUE' });

            } else {

                this.load();
                Alert.alert('Statement', response.data);
                this.setState({ button: 'CONTINUE' });
            }

        } catch (ex) {

            Utils.toast();
            this.setState({ button: 'CONTINUE' });
        }
    }

    render() {

        return (

            <View style={styles.container}>

                <Head title='Statement' navigation={this.props.navigation} back='Home' />

                <Card style={{ backgroundColor: 'transparent', shadowOpacity: 0, elevation: 0 }}>

                    <CardItem header style={styles.cardItem}>
                        <H3 style={{ color: Colors.darkBlue, flex: 1 }}>Date Range</H3>
                    </CardItem>

                    <CardItem style={styles.cardItem}>
                        <Icon name='calendar' style={{ color: Colors.darkBlue }} />
                        <View style={styles.view}>
                            <DatePicker modalTransparent={false} animationType={'slide'} placeHolderText='Select Start Date' placeHolderTextStyle={{ color: Colors.darkBlue }} onDateChange={date => this.setState({ start_date: date })} />
                        </View>
                    </CardItem>

                    <CardItem style={styles.cardItem}>
                        <Icon name='calendar' style={{ color: Colors.darkBlue }} />
                        <View style={styles.view}>
                            <DatePicker modalTransparent={false} animationType={'fade'} placeHolderText='Select End Date' placeHolderTextStyle={{ color: Colors.darkBlue }} onDateChange={date => this.setState({ end_date: date })} />
                        </View>
                    </CardItem>

                    <CardItem style={styles.cardItem}>
                        <Button block success style={{ flex: 1, shadowOpacity: 0, elevation: 0 }} onPress={this.submit}>
                            <Text>{this.state.button}</Text>
                        </Button>
                    </CardItem>

                    <Text style={{ color: Colors.black, fontSize: 11, margin: 15, textAlign: 'center' }}>
                        Please note that your statetment would be emailed to you when ready. Please note that service charge applies.
                            </Text>
                </Card>

                <Separator>
                    <Text style={{ fontSize: 15 }}>Previous Statements</Text>
                </Separator>

                <List noPadding dataArray={this.props.statements} renderRow={row => {

                    return (

                        <ListItem avatar>
                            <Left>
                                <Icon name='attach' style={{ color: Colors.black }} />
                            </Left>
                            <Body>
                                <Text style={{ color: Colors.black }}>{row.status}</Text>
                                <Text>
                                    <Text style={{ color: Colors.red }}>{row.start_date}</Text>
                                    <Text style={{ color: Colors.black }}> - TO - </Text>
                                    <Text style={{ color: Colors.green }}>{row.end_date}</Text>
                                </Text>
                            </Body>
                            <Right>
                                <Text note>{row.created_at}</Text>
                            </Right>
                        </ListItem>
                    )
                }}>
                </List>

            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1
    },
    view: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    cardItem: {
        backgroundColor: 'transparent',
        borderBottomWidth: 1,
        borderBottomColor: Colors.grey
    }
});


const mapStateToProps = state => ({ ...state.auth, ...state.statements });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(Statement);