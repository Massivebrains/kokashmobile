import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View } from 'react-native';
import { Accordion } from 'native-base';
import Head from '../components/Head';
import Spinner from '../components/Spinner';
import API from '../utils/API';
import Utils from '../utils/Utils';
import Colors from '../utils/Colors';
import { FAQ } from '../redux/types';

//user, faq
class Faq extends Component {

    constructor(props) {

        super(props);
    }

    async componentDidMount() {

        try {

            let response = await API.get('faqs', this.props.api_token);

            if (response.status == false) {

                Utils.toast();

            } else {

                this.setProps({ faqs: response.data, loaded: true });
            }

        } catch (ex) {

            Utils.toast(ex.message);
        }
    }

    setProps = (props = {}) => {

        this.props.dispatch({ type: FAQ, payload: props });
    }

    render() {

        if (this.props.faqs.loaded == false) return (<Spinner title='FAQ' navigation={this.props.navigation} />);

        return (

            <View style={styles.container}>

                <Head title='FAQ' navigation={this.props.navigation} back='Home' />

                <Accordion dataArray={this.props.faqs} expanded={0} style={{ color: Colors.darkBlue }} />

            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end'
    }
});

const mapStateToProps = state => ({ ...state.auth, ...state.faq });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(Faq);