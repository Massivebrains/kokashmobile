import React, { Component } from 'react';
import { connect } from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import { View, TouchableOpacity, Alert, StatusBar } from 'react-native';
import { Text } from 'native-base';
import { StackActions, NavigationActions } from 'react-navigation';
import Colors from '../utils/Colors';
import Utils from '../utils/Utils';

class Offline extends Component {

    constructor(props) {

        super(props);
        this.state = { role: null };
    }

    retry = async () => {

        try {

            let network = await NetInfo.getConnectionInfo();

            if (network.type != 'none') {

                let route = this.props.api_token == 'not_set' ? 'Login' : 'Menu';

                this.props.navigation.dispatch(StackActions.reset({ index: 0, key: null, actions: [NavigationActions.navigate({ routeName: route })] }));

            } else {

                Alert.alert('Internet Connectivity', 'Please check your network and try again.');
            }

        } catch (ex) {

            Utils.toast(ex.message);
        }

    }
    render() {

        return (

            <View style={{ backgroundColor: Colors.darkBlue, width: '100%', height: '100%' }}>

                <StatusBar translucent backgroundColor={Colors.darkBlue} barStyle='light-content' animated />

                <View style={{ flexDirection: 'column', padding: 30 }}>
                    <Text style={{ marginTop: '50%', marginBottom: 40, textAlign: 'center', color: Colors.white, fontSize: 25 }}>Can't connect to internet. Please check your network settings!</Text>

                    <TouchableOpacity onPress={() => this.retry()} style={{ backgroundColor: Colors.white, width: '50%', alignSelf: 'center', borderRadius: 50, padding: 0 }}>
                        <View>
                            <Text style={{ color: Colors.darkBlue, textAlign: 'center', padding: 10, fontSize: 18 }}>Try Again</Text>
                        </View>

                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}


const mapStateToProps = state => ({ ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(Offline);
