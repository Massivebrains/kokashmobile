import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Image, Text, View } from 'react-native';
import Head from '../components/Head';
import Colors from '../utils/Colors';

class Contact extends Component {

    render() {

        return (

            <View style={styles.container}>

                <Head title='Contact Us' navigation={this.props.navigation} back='Home' />

                <Text style={{ color: Colors.darkBlue, fontSize: 20, marginTop: 20, textAlign: 'center' }}>{this.props.name}, You Can Reach Us Anytime!</Text>
                <Image source={require('../images/contact.png')} style={{ width: 300, height: 300, alignSelf: 'center' }} />

                <Text style={{ color: Colors.darkBlue, fontSize: 10, textAlign: 'center' }}>Our email is always open</Text>
                <Text style={{ color: Colors.darkBlue, fontSize: 30, textAlign: 'center' }}>{this.props.user.company.contact_person_email}</Text>

                <Text style={{ color: Colors.darkBlue, fontSize: 10, textAlign: 'center', marginTop: 30 }}>or</Text>
                <Text style={{ color: Colors.darkBlue, fontSize: 10, textAlign: 'center' }}>Give Us a Call (Mon - Fri from 9am - 5pm)</Text>
                <Text style={{ color: Colors.darkBlue, fontSize: 30, textAlign: 'center' }}>{this.props.user.company.contact_person_phone}</Text>


            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1
    }
});

const mapStateToProps = state => ({ ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(Contact);