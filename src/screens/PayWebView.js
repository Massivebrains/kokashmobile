import React, { Component } from 'react';
import { connect } from 'react-redux';
import { WebView } from 'react-native-webview';
import { View } from 'react-native';
import Head from '../components/Head';
import Spinner from '../components/Spinner';
import Utils from '../utils/Utils';

class PayWebView extends Component {

    constructor(props) {

        super(props);

        this.state = {

            endpoint: `${Utils.base_url}/webview/pay?api_token=${this.props.api_token}`,
            requesting: true
        }
    }

    async componentDidMount() {

        this.setState({ requesting: false });
    }

    onError = error => {

        this.props.navigation.navigate('Home');
    }

    onMessage = event => {

        this.props.navigation.navigate('Home');
    }

    render() {

        if (this.state.requesting == true) return (<Spinner title='Pay' navigation={this.props.navigation} back='Home' />);

        return (

            <View style={{ flex: 1 }}>

                <Head title='Pay' navigation={this.props.navigation} back='Home' />

                <WebView source={{ uri: this.state.endpoint }} onError={this.onError} onMessage={this.onMessage} scalesPageToFit javascriptEnabled domStorageEnabled startInLoadingState mixedContentMode='always' />

            </View>
        );
    }
}

const mapStateToProps = state => ({ ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(PayWebView);