import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, Text, Linking } from 'react-native';
import Head from '../components/Head';
import Colors from '../utils/Colors';
import KButton from '../components/KButton';
import Spinner from '../components/Spinner';
import Utils from '../utils/Utils';

class TransactionDetail extends Component {

    constructor(props) {

        super(props);

        this.state = {

            transaction: {},
            loading: false
        }
    }

    async componentDidMount() {

        this.load();
        this.props.navigation.addListener('willFocus', this.load);
    }

    load = () => {

        this.setState({ transaction: {}, loading: true });

        let transaction = this.props.navigation.getParam('transaction');

        if (!transaction)
            this.props.navigation.navigate('Transactions');

        this.setState({ transaction: transaction, loading: false });
    }

    componentWillUnmount() {

        this.setState({ transaction: {}, loading: false });
    }

    viewReceipt = () => {

        let url = `${Utils.base_url}/transaction-reciept/${this.state.transaction.id}`;

        Linking.openURL(url).catch(err => {

            Utils.toast(err.toString())
        });
    }

    render() {

        if (this.state.loading == true) return (<Spinner title='Transaction' navigation={this.props.navigation} back='Transactions' />);

        return (

            <View style={{ backgroundColor: Colors.white }}>

                <Head title='Transaction Detail' navigation={this.props.navigation} back='Transactions' />

                <View style={styles.box}>
                    <Text style={styles.p}>{this.state.transaction.amount > 0 ? 'Credit Transaction' : 'Debit Transaction'}</Text>
                    <Text style={styles.h1}>{this.state.transaction.amount_formatted}</Text>
                    <Text style={{ ...styles.p, fontSize: 12 }}>{this.state.transaction.created_at}</Text>
                </View>

                <View style={styles.row}>
                    <Text style={styles.title}>Reference</Text>
                    <Text style={styles.text}>{this.state.transaction.reference}</Text>
                </View>

                <View style={styles.row}>
                    <Text style={styles.title}>Description</Text>
                    <Text style={styles.text}>{this.state.transaction.description}</Text>
                </View>

                <View style={styles.row}>
                    <Text style={styles.title}>Running Balance</Text>
                    <Text style={styles.text}>{this.state.transaction.running_balance_formatted}</Text>
                </View>

                <View style={{ paddingLeft: 10, paddingRight: 10 }}>

                    <KButton onPress={() => this.viewReceipt()} text='VIEW RECIEPT' background={Colors.white} color={Colors.darkBlue}></KButton>

                </View>


            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flexDirection: 'row'
    },

    h1: {
        color: Colors.white,
        fontSize: 30,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    p: {
        textAlign: 'center',
        color: Colors.white
    },
    box: {
        padding: 15,
        justifyContent: 'space-around',
        borderBottomWidth: 1,
        borderBottomColor: Colors.white,
        backgroundColor: Colors.darkBlue
    },
    text: {
        fontSize: 14,
        color: Colors.darkGrey
    },
    title: {
        fontSize: 18,
        color: Colors.darkGrey,
        fontWeight: 'bold'
    },
    listItem: {
        marginTop: 10,
        borderBottomColor: Colors.grey,
        borderBottomWidth: 1
    },

    row: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        padding: 10,
        marginBottom: 10,
        borderBottomColor: Colors.grey,
        borderBottomWidth: 1
    }
});


const mapStateToProps = state => ({ ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(TransactionDetail);