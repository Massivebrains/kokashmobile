import React, { Component } from 'react';
import { connect } from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import { StackActions, NavigationActions } from 'react-navigation';
import SplashScreen from 'react-native-splash-screen';

class Start extends Component {

    async componentWillMount() {

        SplashScreen.hide();
        this.init();
    }

    init = async () => {

        let network = await NetInfo.getConnectionInfo();

        if (network.type == 'none') {

            this.props.navigation.dispatch(StackActions.reset({ index: 0, key: null, actions: [NavigationActions.navigate({ routeName: 'Offline' })] }));
            return;
        }

        let route = 'Swipes';

        try {

            route = this.props.api_token == 'not_set' ? 'Login' : 'Menu';

        } catch (ex) {

        }

        this.props.navigation.dispatch(StackActions.reset({ index: 0, key: null, actions: [NavigationActions.navigate({ routeName: route })] }));

    }

    render() { return null };
}


const mapStateToProps = state => ({ ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(Start);