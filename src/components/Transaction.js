import React from 'react';
import { View, TouchableOpacity, StyleSheet, Text } from 'react-native';
import Colors from '../utils/Colors';

export default class Transaction extends React.Component {

    constructor(props) {

        super(props);

        this.state = {

            transaction: {},
            type: 'Debit',
            color: Colors.red

        }

    }

    componentDidMount() {

        let transaction = this.props.transaction;

        this.setState({ transaction: transaction });

        if (transaction.amount > 0) this.setState({ type: 'Credit', color: Colors.green });
    }

    render() {

        return (

            <TouchableOpacity onPress={() => this.props.navigation.navigate('TransactionDetail', { transaction: this.state.transaction })}>

                <View style={styles.top}>
                    <Text style={styles.h1}>{this.state.transaction.reference}</Text>
                    <Text style={{ color: this.state.color }}>{this.state.transaction.amount > 0 ? '+' : ''} {this.state.transaction.amount_formatted}</Text>
                </View>
                <Text style={styles.description} numberOfLines={1}>
                    {this.state.type}|{this.state.transaction.description}|{this.state.transaction.amount_formatted}|{this.state.transaction.created_at}|{this.state.transaction.status} | {this.state.transaction.reference}
                </Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({

    top: {

        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingBottom: 2,
        paddingRight: 10,
        paddingLeft: 10
    },

    h1: {
        fontSize: 17,
        fontWeight: 'bold',
        color: Colors.black
    },

    description: {
        paddingLeft: 7,
        paddingRight: 7,
        paddingBottom: 7,
        color: Colors.black,
        fontSize: 14,
        borderBottomWidth: 1,
        borderColor: Colors.grey,
        marginBottom: 5
    }

})