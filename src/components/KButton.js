import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';
import { Button } from '@shoutem/ui';
import { DotIndicator } from 'react-native-indicators';
import Colors from '../utils/Colors';

export default class KButton extends Component {

    constructor(props) {

        super(props);
    }

    render() {

        return (

            <Button style={{ padding: this.props.background == Colors.white ? 16 : 17, borderRadius: 4, marginTop: 30, backgroundColor: this.props.background, borderWidth: 1, borderColor: this.props.color }} onPress={this.props.onPress}>

                {this.props.loading
                    ? <DotIndicator size={7} color={this.props.color} style={{ padding: 5 }} />
                    : <Text style={[styles.loading, { color: this.props.color }]}>{this.props.text}</Text>
                }

            </Button>
        )
    }
}


const styles = StyleSheet.create({

    loading: {

        fontWeight: 'bold',
        fontSize: 16,
        textAlign: 'center'
    }
});

