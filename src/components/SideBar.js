import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StatusBar, StyleSheet, View, TouchableOpacity } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { Text, Left, Body, Container, Footer, List, ListItem, Content, Icon, H1, Thumbnail } from 'native-base';
import Colors from '../utils/Colors';
import Utils from '../utils/Utils';
import { LOGOUT } from '../redux/types';

class SideBar extends Component {

    constructor(props) {

        super(props);
    }

    logout = async () => {

        try {

            this.props.dispatch({ type: LOGOUT, payload: null });

        } catch (ex) {

            Utils.toast();
        }

        this.props.navigation.dispatch(StackActions.reset({ index: 0, key: null, actions: [NavigationActions.navigate({ routeName: 'Login' })] }));
    }

    render() {

        return (
            <Container style={{ backgroundColor: Colors.darkBlue }}>
                <StatusBar backgroundColor={Colors.darkBlue} barStyle='light-content' />
                <Content>

                    <TouchableOpacity style={styles.thumbnail} onPress={() => this.props.navigation.navigate('Profile')}>
                        {this.props.user.photo
                            ? <Thumbnail large source={{ uri: this.props.user.photo }} />
                            : <Thumbnail large source={require('../images/avatar.png')} />
                        }

                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')}>
                        <H1 style={styles.h1}>{this.props.name}</H1>
                    </TouchableOpacity>

                    <Text style={styles.h1Text}>{this.props.company} | Member </Text>

                    <List contentContainerStyle={{ marginTop: 50, flex: 1 }} style={{ marginTop: 20 }} >

                        <ListItem icon button noBorder onPress={() => this.props.navigation.navigate('Home')} style={styles.listItem}>
                            <Left>
                                <Icon name='person' style={styles.listItemTextIcon} />
                            </Left>
                            <Body>
                                <Text style={styles.listItemText}>Home</Text>
                            </Body>
                        </ListItem>

                        <ListItem icon button noBorder onPress={() => this.props.navigation.navigate('PayWebView')} style={styles.listItem}>
                            <Left>
                                <Icon name='cart' style={styles.listItemTextIcon} />
                            </Left>
                            <Body>
                                <Text style={styles.listItemText}>Save / Pay Loan</Text>
                            </Body>
                        </ListItem>

                        <ListItem icon button noBorder onPress={() => this.props.navigation.navigate('Loans')} style={styles.listItem}>
                            <Left>
                                <Icon name='pie' style={styles.listItemTextIcon} />
                            </Left>
                            <Body>
                                <Text style={styles.listItemText}>Loans</Text>
                            </Body>
                        </ListItem>

                        <ListItem icon button noBorder onPress={() => this.props.navigation.navigate('Transactions')} style={styles.listItem}>
                            <Left>
                                <Icon name='paper' style={styles.listItemTextIcon} />
                            </Left>
                            <Body>
                                <Text style={styles.listItemText}>Transaction History</Text>
                            </Body>
                        </ListItem>

                        {/* <ListItem icon button noBorder onPress={() => this.props.navigation.navigate('Statement')} style={styles.listItem}>
                            <Left>
                                <Icon name='attach' style={styles.listItemTextIcon} />
                            </Left>
                            <Body>
                                <Text style={styles.listItemText}>Statements</Text>
                            </Body>
                        </ListItem> */}

                        <ListItem icon button noBorder onPress={() => this.props.navigation.navigate('Notifications')} style={styles.listItem}>
                            <Left>
                                <Icon name='notifications' style={styles.listItemTextIcon} />
                            </Left>
                            <Body>
                                <Text style={styles.listItemText}>
                                    Notifications
                                </Text>
                            </Body>
                        </ListItem>

                        <ListItem icon button noBorder onPress={() => this.props.navigation.navigate('Faq')} style={styles.listItem}>
                            <Left>
                                <Icon name='bookmarks' style={styles.listItemTextIcon} />
                            </Left>
                            <Body>
                                <Text style={styles.listItemText}>FAQ</Text>
                            </Body>
                        </ListItem>

                        <ListItem icon button noBorder onPress={() => this.props.navigation.navigate('Contact')} style={styles.listItem}>
                            <Left>
                                <Icon name='bulb' style={styles.listItemTextIcon} />
                            </Left>
                            <Body>
                                <Text style={styles.listItemText}>Contact</Text>
                            </Body>
                        </ListItem>
                    </List>

                </Content>

                <Footer style={{ backgroundColor: 'transparent', flexDirection: 'row', justifyContent: 'space-around' }}>
                    <View style={{ backgroundColor: Colors.white, flex: 1 }}>

                        <Text style={{ color: Colors.darkBlue, textAlign: 'center', marginTop: 15 }} onPress={() => this.logout()}>
                            <Icon name='exit' style={{ fontSize: 15, color: Colors.darkBlue }} /> Logout
                        </Text>
                    </View>
                </Footer>

            </Container >
        );
    }
}

const styles = StyleSheet.create({

    thumbnail: {
        marginTop: 40,
        marginLeft: 15
    },
    h1: {
        marginTop: 10,
        marginLeft: 15,
        color: Colors.white,
        fontSize: 25
    },
    h1Text: {
        marginLeft: 15,
        fontSize: 12,
        color: Colors.white
    },
    listItem: {
        color: Colors.white,
        borderBottomWidth: 0,
        borderBottomColor: '#214360',
        textAlign: 'left',
        marginTop: 5,
        marginBottom: 5
    },
    listItemText: {
        color: Colors.white,
        fontSize: 18
    },
    listItemTextIcon: {
        color: Colors.white
    }
});


const mapStateToProps = state => ({ ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);