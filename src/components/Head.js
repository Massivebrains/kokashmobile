import React from 'react';
import { connect } from 'react-redux';
import { TouchableOpacity } from 'react-native';
import { Header, Left, Right, Icon, Body, Title, Thumbnail } from 'native-base';
import Colors from '../utils/Colors';

class Head extends React.Component {

    constructor(props) {

        super(props);
    }

    render() {

        if (this.props.back != undefined) {

            return (

                <Header style={{ backgroundColor: Colors.darkBlue }} androidStatusBarColor={Colors.darkBlue}>
                    <Left>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate(this.props.back)}>
                            <Icon name='arrow-back' style={{ color: Colors.white }} />
                        </TouchableOpacity>
                    </Left>
                    <Body>
                        <Title>{this.props.title}</Title>
                    </Body>
                    <Right />
                </Header>
            )
        }

        return (

            <Header style={{ backgroundColor: Colors.darkBlue }} androidStatusBarColor={Colors.darkBlue}>
                <Left>
                    <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                        {this.props.user.photo
                            ? <Thumbnail small source={{ uri: this.props.user.photo }} />
                            : <Thumbnail small source={require('../images/avatar.png')} />
                        }
                    </TouchableOpacity>

                </Left>
                <Body>
                    <Title>{this.props.title}</Title>
                </Body>
                <Right />
            </Header>

        );
    }
}

const mapStateToProps = state => ({ ...state.auth });
const mapDispatchToProps = dispatch => { return { dispatch } };

export default connect(mapStateToProps, mapDispatchToProps)(Head);