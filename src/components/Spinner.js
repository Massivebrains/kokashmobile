import React from 'react';
import { StyleSheet, ActivityIndicator } from 'react-native';
import { Container } from 'native-base';
import Head from './Head';
import Colors from '../utils/Colors';

export default class Spinner extends React.Component {

    constructor(props) {

        super(props);
    }

    render() {

        if (this.props.navigation == undefined) {

            return (

                <Container>
                    <ActivityIndicator animating={true} size={50} style={styles.indicator} color={Colors.darkBlue} />
                </Container>
            )
        }

        return (

            <Container>
                <Head title={this.props.title} navigation={this.props.navigation} back={this.props.back} />
                <ActivityIndicator animating={true} size={50} style={styles.indicator} color={Colors.darkBlue} />
            </Container>
        );
    }
}


const styles = StyleSheet.create({

    indicator: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        height: '100%'
    }
});