import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Text, H3, Card, CardItem, Button } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import KButton from './KButton';
import Colors from '../utils/Colors';

export default class Loan extends React.Component {

    constructor(props) {

        super(props);

        this.state = {

            name: '',
            principal: '',
            amount: '',
            paid: '',
            outstanding: '',
            date: '',
            last_payment_date: '',
            status: '',
            loan: {},
            loading: false
        }

    }

    componentDidMount() {

        if (this.props.loan) {

            let loan = this.props.loan;

            this.setState({ loan: loan });
            if (loan.name) this.setState({ name: loan.name });
            if (loan.principal) this.setState({ principal: loan.principal });
            if (loan.amount) this.setState({ amount: loan.amount });
            if (loan.paid) this.setState({ paid: loan.paid });
            if (loan.outstanding) this.setState({ outstanding: loan.outstanding });
            if (loan.date) this.setState({ date: loan.date });
            if (loan.last_payment_date) this.setState({ last_payment_date: loan.last_payment_date });
            if (loan.status) this.setState({ status: loan.status });
        }
    }

    loanDetails = () => {

        this.setState({ loading: true });
        this.props.navigation.navigate('LoanDetails', { loan: this.state.loan });
        this.setState({ loading: false });
    }

    render() {

        return (

            <Card style={{ backgroundColor: 'transparent', shadowOpacity: 0, elevation: 0 }}>
                <LinearGradient colors={['#fff', Colors.grey]} style={{ shadowOpacity: 0, elevation: 0 }}>

                    <CardItem header style={{ backgroundColor: Colors.grey, height: 50, borderBottomWidth: 1, borderBottomColor: '#dae1e7', }}>
                        <View style={styles.view}>
                            <H3 style={{ marginTop: 20, marginBottom: 15, color: Colors.black }}>{this.state.name}</H3>
                        </View>
                    </CardItem>
                    <CardItem style={styles.cardItem}>
                        <View style={styles.view}>
                            <Text style={{ color: Colors.black }}>Principal</Text>
                            <Text style={{ color: Colors.darkBlue }}>{this.state.principal}</Text>
                        </View>
                    </CardItem>

                    <CardItem style={styles.cardItem}>
                        <View style={styles.view}>
                            <Text style={{ color: Colors.black }}>Amount Payable</Text>
                            <Text style={{ color: Colors.darkBlue }}>{this.state.amount}</Text>
                        </View>
                    </CardItem>

                    <CardItem style={styles.cardItem}>
                        <View style={styles.view}>
                            <Text style={{ color: Colors.green }}>Paid</Text>
                            <Text style={{ color: Colors.green }}>{this.state.paid}</Text>
                        </View>
                    </CardItem>

                    <CardItem style={styles.cardItem}>
                        <View style={styles.view}>
                            <Text style={{ color: Colors.red }}>Outstanding</Text>
                            <Text style={{ color: Colors.red }}>{this.state.outstanding}</Text>
                        </View>
                    </CardItem>

                    <CardItem style={styles.cardItem}>
                        <View style={styles.view}>
                            <Text style={{ color: Colors.black }}>Date Started</Text>
                            <Text style={{ color: Colors.black }}>{this.state.date}</Text>
                        </View>
                    </CardItem>

                    <CardItem style={styles.cardItem}>
                        <View style={styles.view}>
                            <Text style={{ color: Colors.black }}>Last Payment Date</Text>
                            <Text style={{ color: Colors.black }}>{this.state.last_payment_date}</Text>
                        </View>
                    </CardItem>

                    <CardItem style={styles.cardItem}>
                        <View style={styles.view}>
                            <Text style={{ color: Colors.black }}>Status</Text>
                            <Text style={{ color: Colors.black }}>{this.state.status}</Text>
                        </View>
                    </CardItem>

                    <View style={{ padding: 10 }}>
                        <KButton onPress={() => this.loanDetails()} text='VIEW DETAILS' loading={this.state.loading} background={Colors.darkBlue} color={Colors.white}></KButton>
                    </View>

                </LinearGradient>
            </Card>
        );
    }
}

const styles = StyleSheet.create({

    text: {
        textAlign: 'right'
    },
    view: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    cardItem: {
        backgroundColor: 'transparent',
        height: 30,
        borderBottomWidth: 1,
        borderBottomColor: '#dae1e7'

    }
});
