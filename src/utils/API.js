const Utils = require('./Utils');

module.exports = {

    get: (endpoint = '', token = '') => {

        return new Promise(async (resolve, reject) => {

            let payload = {

                headers: {

                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            };

            fetch(`${Utils.api_base_url}/${endpoint}`, payload).then(response => response.json()).then(json => {

                resolve(json);

            }).catch(error => {

                reject(error);

            });
        });

    },

    post: (endpoint = '', data = {}, token = '') => {

        return new Promise(async (resolve, reject) => {

            let payload = {

                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify(data),
            };

            fetch(`${Utils.api_base_url}/${endpoint}`, payload).then(response => response.json()).then(json => {

                resolve(json);

            }).catch(error => {

                reject(error);

            });
        });
    }
}