import { ToastAndroid } from 'react-native';
let base_url = 'https://kokash.ng';
//let base_url = 'http://33540779.ngrok.io';

module.exports = {

    base_url: base_url,
    api_base_url: `${base_url}/api`,
    currency_symbol: 'NGN',
    error_message: 'Could not connect at this time. Check your internet or logout and login again.',
    toast: (text = 'Could not connect at this time. Check your internet or logout and login again.') => ToastAndroid.showWithGravity(text, ToastAndroid.SHORT, ToastAndroid.BOTTOM)
}