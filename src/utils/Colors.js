module.exports = {

    blue: '#2779bd',
    lightDarkBlue: '#365e82',
    darkBlue: '#1c3d5a',
    yellow: '#fff',
    red: '#e3342f',
    green: '#38c172',
    white: '#fff',
    grey: '#f1f5f8',
    greyDark1: '#d0d4d6',
    darkGrey: '#606f7b',
    black: '#3d4852',
    light: 'rgba(0,0,0,0.01)',
    lightBlue: '#4a7ba8'
}