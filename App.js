import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { StyleSheet, ActivityIndicator, View } from 'react-native';
import { Component } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './src/redux/store';

import Init from './Init';
import Start from './src/screens/Start';
import Login from './src/screens/begin/Login';
import Swipes from './src/screens/begin/Swipes';
import Password from './src/screens/begin/Password';
import Home from './src/screens/Home';
import PayWebView from './src/screens/PayWebView';
import Loans from './src/screens/Loans';
import LoanDetails from './src/screens/LoanDetails';
import Transactions from './src/screens/Transactions';
import TransactionDetail from './src/screens/TransactionDetail';
import Statement from './src/screens/Statement';
import Notifications from './src/screens/Notifications';
import Faq from './src/screens/Faq';
import Contact from './src/screens/Contact';
import Profile from './src/screens/Profile';
import Offline from './src/screens/Offline';

import SideBar from './src/components/SideBar';

import Colors from './src/utils/Colors';

const Menu = createDrawerNavigator(
  {
    Home: { screen: Home, },
    Contact: { screen: Contact },
    Faq: { screen: Faq },
    Notifications: { screen: Notifications },
    Statement: { screen: Statement },
    Loans: { screen: Loans },
    LoanDetails: { screen: LoanDetails },
    PayWebView: { screen: PayWebView },
    Transactions: { screen: Transactions },
    TransactionDetail: { screen: TransactionDetail },
    Profile: { screen: Profile }
  },
  {
    contentComponent: props => <SideBar {...props} />,
    drawerBackgroundColor: 'transparent'
  }
)

const Nav = createStackNavigator(
  {
    Init: { screen: Init },
    Start: { screen: Start },
    Swipes: { screen: Swipes },
    Menu: Menu,
    Login: { screen: Login },
    Password: { screen: Password },
    Offline: { screen: Offline }

  },
  {
    headerMode: 'none',
    navigationOptions: { drawerLockMode: 'locked-closed' }
  }
);

const RootApp = createAppContainer(Nav);

export default class App extends Component {

  loading = () => {

    return (

      <View style={styles.container}>
        <ActivityIndicator size='large' color={Colors.darkBlue} />
      </View>
    )
  }

  render() {

    return (

      <Provider store={store}>
        <PersistGate persistor={persistor} loading={this.loading()}>
          <RootApp />
        </PersistGate>
      </Provider>

    );
  }
}


const styles = StyleSheet.create({

  container: {

    flex: 1,
    backgroundColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

